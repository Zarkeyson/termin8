package studentskaSluzba;

public class Profesor extends Osoba {
	private int godinaZaposlenja;
	private int brojPredmeta;
	
	public Profesor() {}

	public Profesor(String ime, String prezime, int godinaRodjenja, String jmbg, int godinaZaposlenja,
			int brojPredmeta) {
		super(ime, prezime, godinaRodjenja, jmbg);
		this.godinaZaposlenja = godinaZaposlenja;
		this.brojPredmeta = brojPredmeta;
	}

	public int getGodinaZaposlenja() {
		return godinaZaposlenja;
	}

	public void setGodinaZaposlenja(int godinaZaposlenja) {
		this.godinaZaposlenja = godinaZaposlenja;
	}

	public int getBrojPredmeta() {
		return brojPredmeta;
	}

	public void setBrojPredmeta(int brojPredmeta) {
		this.brojPredmeta = brojPredmeta;
	}

	@Override
	public String toString() {
		return "Profesor [godinaZaposlenja=" + godinaZaposlenja + ", brojPredmeta=" + brojPredmeta + ", ime=" + ime
				+ ", prezime=" + prezime + ", godinaRodjenja=" + godinaRodjenja + ", jmbg=" + jmbg + "]";
	}
	
	
}
