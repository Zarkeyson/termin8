package studentskaSluzba;

public class Student extends Osoba {
	private String brojIndeksa;
	private String smer;
	
	public Student() {}

	public Student(String ime, String prezime, int godinaRodjenja, String jmbg, String brojIndeksa, String smer) {
		super(ime, prezime, godinaRodjenja, jmbg);
		this.brojIndeksa = brojIndeksa;
		this.smer = smer;
	}

	public String getBrojIndeksa() {
		return brojIndeksa;
	}

	public void setBrojIndeksa(String brojIndeksa) {
		this.brojIndeksa = brojIndeksa;
	}

	public String getSmer() {
		return smer;
	}

	public void setSmer(String smer) {
		this.smer = smer;
	}

	@Override
	public String toString() {
		return "Student [brojIndeksa=" + brojIndeksa + ", smer=" + smer + ", ime=" + ime + ", prezime=" + prezime + ", godinaRodjenja=" + godinaRodjenja + ", jmbg=" + jmbg + "]";
	}
}
