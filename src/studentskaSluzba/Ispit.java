package studentskaSluzba;

import java.util.ArrayList;
import java.util.List;

public class Ispit implements BrojStudenata {
	private String datumOdrzavanja;
	private String nazivPredmeta;
	private Profesor profesor;
	private List<Student> stundeti;
	
	public Ispit() {}

	public Ispit(String datumOdrzavanja, String nazivPredmeta, Profesor profesor, List<Student> stundeti) {
		super();
		this.datumOdrzavanja = datumOdrzavanja;
		this.nazivPredmeta = nazivPredmeta;
		this.profesor = profesor;
		this.stundeti = stundeti;
	}

	public Ispit(String datumOdrzavanja, String nazivPredmeta, Profesor profesor) {
		super();
		this.datumOdrzavanja = datumOdrzavanja;
		this.nazivPredmeta = nazivPredmeta;
		this.profesor = profesor;
		this.stundeti = new ArrayList<>();
	}

	public String getDatumOdrzavanja() {
		return datumOdrzavanja;
	}

	public void setDatumOdrzavanja(String datumOdrzavanja) {
		this.datumOdrzavanja = datumOdrzavanja;
	}

	public String getNazivPredmeta() {
		return nazivPredmeta;
	}

	public void setNazivPredmeta(String nazivPredmeta) {
		this.nazivPredmeta = nazivPredmeta;
	}

	public Profesor getProfesor() {
		return profesor;
	}

	public void setProfesor(Profesor profesor) {
		this.profesor = profesor;
	}

	public List<Student> getStundeti() {
		return stundeti;
	}

	public void setStundeti(List<Student> stundeti) {
		this.stundeti = stundeti;
	}
	
	public void dodajStudenta(Student student) {
		this.stundeti.add(student);
	}
	
	public int izracunajBrojStudenata() {
		return this.stundeti.size();
	}
	
	public void obrisiStudenta(String brojIndeksa) {
		/*for (Student s : this.stundeti) {
			if (s.getBrojIndeksa().equals(brojIndeksa)) {
				this.stundeti.remove(s);
			}
		}*/
		int indeks = -1;
		for (int i = 0; i < this.stundeti.size(); i++) {
			if (this.stundeti.get(i).getBrojIndeksa().equals(brojIndeksa)) {
				indeks = i;
			}
		}
		if (indeks != -1) {
			this.stundeti.remove(indeks);
		}
	} 
	
	@Override
	public String toString() {
		String ispis = "";
		ispis += "Datum odrzavanja: " + this.datumOdrzavanja;
		ispis += "\nNaziv: " + this.nazivPredmeta;
		ispis += "\nProfesor: " + this.profesor;
		for (Student s : this.stundeti) {
			ispis += "\n\tStudent: " + s;
		}
		return ispis;
	}
}
