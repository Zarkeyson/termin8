package studentskaSluzba;

public class Osoba {
	protected String ime;
	protected String prezime;
	protected int godinaRodjenja;
	protected String jmbg;
	
	public Osoba() {}

	public Osoba(String ime, String prezime, int godinaRodjenja, String jmbg) {
		super();
		this.ime = ime;
		this.prezime = prezime;
		this.godinaRodjenja = godinaRodjenja;
		this.jmbg = jmbg;
	}

	@Override
	public String toString() {
		return "Osoba [ime=" + ime + ", prezime=" + prezime + ", godinaRodjenja=" + godinaRodjenja + ", jmbg=" + jmbg
				+ "]";
	}

	public String getIme() {
		return ime;
	}

	public void setIme(String ime) {
		this.ime = ime;
	}

	public String getPrezime() {
		return prezime;
	}

	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

	public int getGodinaRodjenja() {
		return godinaRodjenja;
	}

	public void setGodinaRodjenja(int godinaRodjenja) {
		if (godinaRodjenja < 1920) {
			this.godinaRodjenja = 1920;
		} else {
			this.godinaRodjenja = godinaRodjenja;
		}
	}

	public String getJmbg() {
		return jmbg;
	}

	public void setJmbg(String jmbg) {
		this.jmbg = jmbg;
	}
	
	
	
}
