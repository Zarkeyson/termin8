package studentskaSluzba;

import java.util.ArrayList;
import java.util.List;

public class StudentskaSluzba {

	public static void main(String[] args) {
		
		Osoba o1 = new Osoba();
		//o1.ime = "Pera";
		//o1.prezime = "Peric";
		//o1.godinaRodjenja = 1900;
		//o1.jmbg = "0123456789";
		o1.setIme("Pera");
		o1.setPrezime("Peric");
		o1.setGodinaRodjenja(1900);
		o1.setJmbg("0123456789");
		System.out.println(o1);
		
		Osoba o2 = new Osoba("Zika", "Zikic", 1998, "9876543210");
		System.out.println(o2);
		
		Osoba o3 = new Osoba("Mika", "Mikic", 1992, "123456654321");
		System.out.println(o3);
		
		Osoba o4 = new Osoba("Kevin", "Punter", 1999, "1234554321");
		System.out.println(o4);
		
		Student s1 = new Student("Stefan", "Colic", 1995, "123456987654", "sw-5/2014", "Softversko inzenjerstvo i informacione tehnologije");
		System.out.println(s1);
		
		Student s2 = new Student("Tijana", "Prokic", 1996, "999888777444", "sw-6/2014", "Merenje i Regulacija");
		System.out.println(s2);
		
		Profesor p1 = new Profesor("Goran", "Sladic", 1980, "123654987", 2005, 5);
		System.out.println(p1);
		
		System.out.println("---------------------");
		// Nacin 1
		Ispit i1 = new Ispit("04-05-2023", "Matematicka Analiza 1", p1);
		i1.dodajStudenta(s1);
		i1.dodajStudenta(s2);
		System.out.println(i1);
		
		System.out.println("---------------------");
		// Nacin 2
		List<Student> studentiKojiPolazuIspit = new ArrayList<>();
		studentiKojiPolazuIspit.add(s1);
		studentiKojiPolazuIspit.add(s2);
		Ispit i2 = new Ispit("04-05-2023", "Sociologija tehnike", p1, studentiKojiPolazuIspit);
		System.out.println(i2);
		
		System.out.println("---------------------");
		i2.obrisiStudenta("sw-5/2014");
		System.out.println(i2);
	}

}
